# Deployment Guidelines

This is markdown template to documenting product deployment step.


# Projects 

Describe all projects information and description on this section

## Requirements

All requirements need to deploy the product 
> **Example:** 
dependency requirements. (Jenkins,Java,NPM,Python 3.xxx,Docker)


## Environtment Setup

Put all information related server/env setup guide on this section.
>**Example:**
Server configuration
Network configuration
etc.

## Deployment Workflow
Describe Deployment Step (Initial/Update/Rollback) on this section.

>**Example** Bellow is an example sentry initial deployment step in cloud foundry, for more detail.
### Initial Steps

1.  Target the space in Cloud Foundry that you want to deploy your app to:  
    For example:  `cf target -o sentry -s sentry`
2.  Clone the  `sentry-on-cf`  repo:  
    `git clone https://github.com/dlapiduz/sentry-on-cf`
3.  Edit the  `manifest.yml`  file to match your desired  `host`  and  `domain`.
4.  Push the application without starting it to create the environment:  
    `cf push --no-start`
5.  Create a postgres database service instance and bind it:  
    For example:  `cf create-service rds shared-psql sentrydb`  
    `cf bind-service sentry sentrydb`
6.  Create a redis service instance and bind it:  
    For example:  `cf create-service redis28-swarm standard sentryredis`  
    `cf bind-service sentry sentryredis`
7.  Add the services to your  `manifest.yml`  file:

```
...
services:
- sentrydb
- sentryredis
...

```

1.  Get the Redis URL from the app environment and take note of it:  `cf env sentry`
2.  Set required environment variables in your  `manifest.yml`  file:  
    (use the Redis settings from the previous step to set REDIS_URL)

```
env:
  SENTRY_URL_PREFIX: my-sentry.apps.com
  REDIS_URL: redis://dummy:password@1.1.1.1:12345
  SECRET_KEY: secretkeyshhh
  SENTRY_ADMIN_EMAIL: myemail@email.com
  SERVER_EMAIL: theserver@email.com
  MANDRILL_USERNAME:  
  MANDRILL_APIKEY:  

```

1.  Run  `cf-ssh`  to ssh into a container to run admin tasks (this might take a while)
2.  Once in the container terminal run:

```
# Fill in the DB
sentry --config=sentry.conf.py upgrade --noinput
# Create an account (say yes to superuser)
sentry --config=sentry.conf.py createuser

```

1.  Do one more  `cf push`  to make sure all the apps are up and running



>**Notes:**  Follow above format to describe **Update** and **Rollback** step deployment workflow.

>**CI/CD** If you are using CI/CD please describe all the process and the pipelines script.


